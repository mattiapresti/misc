from time import sleep

from selenium import webdriver
from selenium.webdriver.firefox.options import Options
import telegram_send

fiscal_code = "XXXXXXXXXXXXXXXX"
last_digits = "XXXXXX"
website = 'https://vaccinicovid.regione.veneto.it/ulssX'

options = Options()
options.add_argument('--headless')

driver = webdriver.Firefox(options=options)
driver.get(website)

fiscal_code_box = driver.find_element_by_xpath('//*[@id="cod_fiscale"]')
fiscal_code_box.send_keys(fiscal_code)

last_digits_box = driver.find_element_by_xpath('//*[@id="num_tessera"]')
last_digits_box.send_keys(last_digits)

data_consent_button = driver.find_element_by_xpath('/html/body/div/form/div[2]/div[4]/div[2]/input')
data_consent_button.click()

confirmation_button = driver.find_element_by_xpath('/html/body/div/form/div[2]/div[6]/div[2]/button')
confirmation_button.click()

sleep(2)

category_button = driver.find_element_by_xpath('/html/body/div/form/div[3]/button[1]')
category_button.click()

sleep(2)

locations_div = driver.find_element_by_id('corpo1')
location_buttons = locations_div.find_elements_by_class_name('btn-primary')[:-1] # last button is not a location

locations_available = []
for button in location_buttons:
    if not button.text.find('DISPONIBILITA ESAURITA'):
        locations_available.append(button.text)

driver.close()

message = []
if len(locations_available) > 0:
    message.append(f"Ci sono dei posti disponibili! {website}")
    message.extend(locations_available)
    telegram_send.send(messages=message)
